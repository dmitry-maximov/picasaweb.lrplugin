--local LrMobdebug = import 'LrMobdebug'    
local LrLogger = import 'LrLogger'
local log = LrLogger( 'PicasaLogger')
--require 'debug'
if pcall( function() require 'debug' end) then
  log:enable("print")
else
  log:disable()
end
    -- Lightroom SDK
local LrBinding = import 'LrBinding'
local LrDialogs = import 'LrDialogs'
local LrFileUtils = import 'LrFileUtils'
local LrPathUtils = import 'LrPathUtils'
local LrView = import 'LrView'
local LrHttp = import 'LrHttp'
local LrErrors = import 'LrErrors'

local prefs = import 'LrPrefs'.prefsForPlugin()

require 'defs'
local PicasaAPI = require('PicasaAPI')
local PicasaPublishSupport = require 'PicasaPublishSupport'

local inspect = require('inspect')

    -- Common shortcuts
local bind = LrView.bind
local share = LrView.share


  -- Plugin presets
local exportServiceProvider = {}

for name, value in pairs( PicasaPublishSupport ) do
    exportServiceProvider[ name ] = value
end

exportServiceProvider.supportsIncrementalPublish = true

exportServiceProvider.exportPresetFields = {
    { key = 'PhotoTitle', default = 'filename' },
    { key = 'PhotoSummary', default = 'titleAndCaption' },
    { key = 'OpenBrowser', default = true },
    { key = 'Account', default = LOC "$$$/Picasa/ExportDialog/Title/NotLoggedIn=Not logged in" },
    { key = 'AccountID', default = nil },
    { key = 'AccountInfo', default = '' },
    { key = 'LoginButtonTitle', default = loginButtonChoice.login},
    { key = 'ClientID', default = '263500772157-ho57gvsmirtd86gu9783fed33mvkiho5.apps.googleusercontent.com'},
    { key = 'ClientSecret', default = 'Kp6QNBO7lIf4Ml92ZUhkWLDx'},
    { key = 'AccessToken', default = nil},
    { key = 'RefreshToken', default = nil},
    { key = 'ValidAccount', default = false},
    { key = 'ValidLicense', default = true},
    { key = 'Albums', default = { } }
}

exportServiceProvider.hideSections = { 'exportLocation', 'video' }
exportServiceProvider.allowFileFormats = { 'JPEG' }
exportServiceProvider.allowColorSpaces = { 'sRGB' }
exportServiceProvider.hidePrintResolution = true
exportServiceProvider.canExportVideo = false


exportServiceProvider.updateExportSettings = function( settings ) 
  if not settings.ValidLicense then
    settings.LR_size_doConstrain = true 
    settings.LR_size_doNotEnlarge = true 
    settings.LR_size_maxHeight = 800
    settings.LR_size_maxWidth = 800
    settings.LR_size_resizeType = "wh"
  end
end


  -- Plugin functions
function exportServiceProvider.startDialog( propertyTable )
  --propertyTable:addObserver( 'validAccount', function() updateCantExportBecause( propertyTable ) end )
	--updateCantExportBecause( propertyTable )
  log:trace('startDialog')
	PicasaAPI.verifyLogin( propertyTable )
end

function exportServiceProvider.sectionsForTopOfDialog( f, propertyTable )
    log:trace('sectionsForTopOfDialog')
    
    return {
    
        {
            title = LOC "$$$/Picasa/ExportDialog/AccountTitle=Picasa Account",
            synopsis = bind 'Account',

            f:row {
                spacing = f:control_spacing(),

                f:static_text {
                    title = LOC "$$$/Picasa/ExportDialog/Account=Account:",
                    alignment = 'right',
                    width = share 'labelWidth',
                },
                
                f:static_text {
                    title = bind 'Account',
                    width = 200,
                },

                f:push_button {
                    width = 90,
                    title = bind 'LoginButtonTitle',
                    --enabled = bind 'loginButtonEnabled',
                    action = function()
                      if propertyTable.LoginButtonTitle == loginButtonChoice.login then
                        PicasaAPI.showGetCodeDialog(propertyTable)
                        PicasaAPI.getAccessAndRefreshToken(propertyTable)
                      else
                        PicasaAPI.notLoggedIn(propertyTable)
                      end
                    --get
                    end,
                },

            },

            --[[f:row {
                spacing = f:control_spacing(),

                f:static_text {
                    title = LOC "$$$/Picasa/ExportDialog/AccountInfo=Account Info:",
                    alignment = 'right',
                    width = share 'labelWidth',
                },

                f:static_text {
                    title = bind 'AccountInfo',
                    width = 200,
                },
                
            }]]--
        },

        {
            title = LOC "$$$/Picasa/ExportDialog/SettingsTitle=Picasa Settings",

            --[[synopsis = bind 'CurrentAlbum',
            
            f:row {
                spacing = f:control_spacing(),

                f:static_text {
                    title = LOC "$$$/Picasa/ExportDialog/Album=Album:",
                    alignment = 'right',
                    width = share 'labelWidth',
                },
                f:popup_menu{
                    width = 250,
                    value = bind 'CurrentAlbum',
                    items = bind 'Albums',
                    visible = bind 'ValidAccount'                    
                },
                f:push_button {
                    --width = 120,
                    title = LOC "$$$/Picasa/ExportDialog/CreateAlbum=Create new album",
                    action = function()
                      PicasaAPI.showCreateAlbumDialog(propertyTable)
                    end,
                    visible = bind 'ValidAccount'
                },
            },]]--
            f:row{
                spacing = f:control_spacing(),

                f:static_text {
                    title = LOC "$$$/Picasa/ExportDialog/PhotoTitle=Photo title:",
                    alignment = 'right',
                    width = share 'labelWidth',
                },

                f:popup_menu {
                    --width = 180,
                    value = bind 'PhotoTitle',
                    items = {
                            { value = 'title', title = photoTitleChoice.title },
                            { value = 'filename', title = photoTitleChoice.filename }
                    },
                },

            },
            f:row{
                spacing = f:control_spacing(),

                f:static_text {
                    title = LOC "$$$/Picasa/ExportDialog/PhotoSummary=Photo summary:",
                    alignment = 'right',
                    width = share 'labelWidth',
                },

                f:popup_menu {
                    --width = 180,
                    value = bind 'PhotoSummary',
                    items = {
                            { value = 'title', title = photoTitleChoice.title },
                            { value = 'caption', title = photoTitleChoice.caption },
                            { value = 'titleAndCaption', title = photoTitleChoice.titleAndCaption },
                            { value = 'filename', title = photoTitleChoice.filename },
                            { value = 'filenameNoExt', title = photoTitleChoice.filenameNoExt },
                    },
                },
            },
            f:row{
                spacing = f:control_spacing(),

                f:static_text {
                    title = LOC "$$$/Picasa/ExportDialog/OpenBrowser=Open browser after export:",
                    alignment = 'right',
                    width = share 'labelWidth',
                },

                f:checkbox {
                    value = bind 'OpenBrowser',
                },

            }

        }
    }
end


function exportServiceProvider.processRenderedPhotos( functionContext, exportContext )
  
  local exportSession = exportContext.exportSession
	-- Make a local reference to the export parameters.
	local exportSettings = assert( exportContext.propertyTable )
	-- Get the # of photos.
	local nPhotos = exportSession:countRenditions()
	-- Set progress title.
	local progressScope = exportContext:configureProgress {
						title = nPhotos > 1
									and LOC( "$$$/Picasa/Publish/Progress=Publishing ^1 photos to Picasa", nPhotos )
									or LOC "$$$/Picasa/Publish/Progress/One=Publishing one photo to Picasa",
					}
          
  -- Todo: promt to create album if not exists  
  local publishedCollectionInfo = exportContext.publishedCollectionInfo
  
  local albumID
  if publishedCollectionInfo == nil then
    albumID = PicasaAPI.showSelectOrCreateAlbumDialog(exportSettings)
  else
    albumID = publishedCollectionInfo.remoteId
    PicasaAPI.updateStatus(exportSettings)
  end
    
  log:trace('Current album', albumID)
          
  for i, rendition in exportContext:renditions { stopIfCanceled = true } do
    
    -- Update progress scope.
		progressScope:setPortionComplete( ( i - 1 ) / nPhotos )
		
		-- Get next photo.
		local photo = rendition.photo
    
    local success, pathOrMessage = rendition:waitForRender()
    progressScope:setPortionComplete( ( i - 0.5 ) / nPhotos )
    
    -- Check for cancellation again after photo has been rendered.
    if progressScope:isCanceled() then 
      LrFileUtils.delete( pathOrMessage )
      break 
    end
    
    if success then
      local title = PicasaAPI.getPhotoTitle( photo, exportSettings.PhotoTitle, pathOrMessage )
      local summary = PicasaAPI.getPhotoTitle( photo, exportSettings.PhotoSummary, pathOrMessage )
      
      if publishedCollectionInfo == nil then --Export service
        PicasaAPI.uploadPhoto({title=title, summary=summary, filePath=pathOrMessage, albumID=albumID}, exportSettings.AccountID, exportSettings.AccessToken)
      else --Publish service
        local uploadedId = rendition.publishedPhotoId
        if uploadedId == nil then --Uploading for the first time
          uploadedId = PicasaAPI.uploadPhoto({title=title, summary=summary, filePath=pathOrMessage, albumID=albumID}, exportSettings.AccountID, exportSettings.AccessToken)
        else
          uploadedId = PicasaAPI.updatePhoto({title=title, summary=summary, filePath=pathOrMessage, photoId=uploadedId}, exportSettings.AccountID, exportSettings.AccessToken)
        end
        rendition:recordPublishedPhotoId( PicasaAPI.constructPhotoID(exportSettings.AccountID, albumID, uploadedId ))
        rendition:recordPublishedPhotoUrl( string.format(PicasaAPI.googleAuth.photoPlusUrl, exportSettings.AccountID, albumID, uploadedId ))
      end
    else
      rendition:uploadFailed( pathOrMessage )
    end
    LrFileUtils.delete( pathOrMessage )
    
  end

  if exportSettings.OpenBrowser then
    LrHttp.openUrlInBrowser(string.format(PicasaAPI.googleAuth.albumPlusUrl, exportSettings.AccountID, albumID))
  end 
  
  progressScope:done()
end

return exportServiceProvider