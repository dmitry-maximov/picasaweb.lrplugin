local LrLogger = import 'LrLogger'
local log = LrLogger( 'PicasaLogger' )
    
    -- Lightroom SDK
local LrBinding = import 'LrBinding'
local LrDialogs = import 'LrDialogs'
local LrFunctionContext = import 'LrFunctionContext'
local LrFileUtils = import 'LrFileUtils'
local LrPathUtils = import 'LrPathUtils'
local LrView = import 'LrView'
local LrHttp = import 'LrHttp'
local LrErrors = import 'LrErrors'
local LrTasks = import "LrTasks"

local json = require ("dkjson")
require 'defs'
local xml = require('xml')
--local inspect = require('inspect')

local prefs = import 'LrPrefs'.prefsForPlugin()

local bind = LrView.bind
local share = LrView.share

local PicasaAPI = {}

PicasaAPI.googleAuth = {
    authURL1 = 'https://accounts.google.com/o/oauth2/auth?client_id=%s',
    authURL2 = '&redirect_uri=urn%3Aietf%3Awg%3Aoauth%3A2.0%3Aoob&response_type=code&scope=https%3A%2F%2Fpicasaweb.google.com%2Fdata%2F%20profile%20email',
    accessURL = 'https://accounts.google.com/o/oauth2/token',
    accessBody1 = 'code=%s&client_id=%s&client_secret=%s',
    accessBody2 = '&redirect_uri=urn%3Aietf%3Awg%3Aoauth%3A2.0%3Aoob&grant_type=authorization_code',
    refreshBody = 'refresh_token=%s&client_id=%s&client_secret=%s&grant_type=refresh_token',
    userinfoURL = 'https://www.googleapis.com/oauth2/v1/userinfo',
    userapiURL = 'https://picasaweb.google.com/data/feed/api/user/%s',
    albumUrl = 'https://picasaweb.google.com/data/feed/api/user/%s/albumid/%s',
    albumPutUrl = 'https://picasaweb.google.com/data/entry/api/user/%s/albumid/%s',
    albumPlusUrl = 'https://plus.google.com/photos/%s/albums/%s',
    photoPlusUrl = 'https://plus.google.com/photos/%s/albums/%s/%s',
    photoPutUrl = 'https://picasaweb.google.com/data/media/api/user/%s/albumid/%s/photoid/%s',
    photoDelUrl = 'https://picasaweb.google.com/data/entry/api/user/%s/albumid/%s/photoid/%s',
    photoCommentUrl = 'https://picasaweb.google.com/data/feed/api/user/%s/albumid/%s/photoid/%s'
}

local function formatError( nativeErrorCode )
	return LOC( "$$$/Picasa/Error/NetworkFailure=Could not contact the Google ^1", nativeErrorCode)
end


local function trim( s )
	return string.gsub( s, "^%s*(.-)%s*$", "%1" )
end

function split(str, sep)
  local sep, fields = sep or ":", {}
  local pattern = string.format("([^%s]+)", sep)
  str:gsub(pattern, function(c) fields[#fields+1] = c end)
  return fields
end


function PicasaAPI.showGetCodeDialog(propertyTable)
    log:trace ("PicasaAPI.showGetCodeDialog")
    
    LrFunctionContext.callWithContext( 'PicasaAPI.showGetCodeDialog', function( context )
      local f = LrView.osFactory()
      local properties = LrBinding.makePropertyTable( context )
      
      local contents = f:column {
        bind_to_object = properties,
        spacing = f:control_spacing(),
        fill = 1,
    
        f:static_text {
          title = LOC "$$$/Picasa/LoginDialog/Message=In order to use this picasa plug-in, you must obtain an Access code from google. Sign on to Google and enter code into field below.",
          fill_horizontal = 1,
          width_in_chars = 55,
          height_in_lines = 2,
          size = 'small',
        },
    
        f:row {
          spacing = f:label_spacing(),
          
          f:static_text {
            title = LOC "$$$/Picasa/LoginDialog/Code=Access code:",
            alignment = 'right',
            width = share 'title_width',
          },
          
          f:edit_field { 
            fill_horizonal = 1,
            width_in_chars = 35, 
            value = bind 'AccessCode',
          }
        }
      }
      local result = LrDialogs.presentModalDialog {
          title = LOC "$$$/Picasa/LoginDialog/Title=Enter Your Google Access code", 
          contents = contents,
          accessoryView = f:push_button {
            title = LOC "$$$/Picasa/LoginDialog/GoToGoogle=Get Google Access code...",
            action = function()
              local url = string.format(PicasaAPI.googleAuth.authURL1, propertyTable.ClientID)..PicasaAPI.googleAuth.authURL2
              log:trace('openUrlInBrowser', url)
              LrHttp.openUrlInBrowser( url )
            end
          },
        }
      
      if result == 'ok' then
        prefs.AccessCode = trim(properties.AccessCode)
      else
        LrErrors.throwCanceled()
      end
    end )
end

function PicasaAPI.showSelectOrCreateAlbumDialog(propertyTable)
    log:trace ("PicasaAPI.showSelectOrCreateAlbumDialog")
    
    local albumID
    LrFunctionContext.callWithContext( 'PicasaAPI.showSelectOrCreateAlbumDialog', function( context )
      local f = LrView.osFactory()
      local properties = LrBinding.makePropertyTable( context )
      properties['SelectedMode'] = 'existing'
      properties['SelectedAlbum'] = nil
      
      
      LrTasks.startAsyncTask( function()
        local info = PicasaAPI.getAccountDetails( propertyTable )
        log:trace('Total entries', #info.entry)
        
        local albums = {}
        
        local currentFound = false
        for i=1, #info.entry do
          local id = tostring(info.entry[i].id)
          albums[#albums+1] = {value=id, title=tostring(info.entry[i].title)}
          --[[if propertyTable.LastSelectedAlbum == id then
            currentFound = true
          end]]--
        end
        properties.Albums = albums
        
        --if propertyTable.LastSelectedAlbum == nil or not currentFound then
        if #albums > 0 then
          properties.SelectedAlbum = albums[1].value
        end
      end )
      
      local contents = f:column {
        bind_to_object = properties,
        spacing = f:control_spacing(),
        --fill = 1,
        
        f:row {
            spacing = f:control_spacing(),
            
            f:radio_button {
              title = LOC "$$$/Picasa/ExportDialog/Existing Album=Existing album:",
              checked_value = "existing",
              value = bind 'SelectedMode',
              alignment = 'right',
              width = share 'labelWidth'
            },
            
            f:popup_menu{
                width = 250,
                value = bind 'SelectedAlbum',
                items = bind 'Albums',
                enabled = bind {
                  key = 'SelectedMode',
                  transform = function( value, fromTable )
                    return value == 'existing'
                  end
                }
            }
                
        },
        
        f:row {
            spacing = f:control_spacing(),
            
            f:radio_button {
              title = LOC "$$$/Picasa/ExportDialog/New Album=Create new album:",
              checked_value = "new",
              value = bind 'SelectedMode',
              alignment = 'right',
              width = share 'labelWidth'
            },
            
            f:edit_field { 
            --fill_horizonal = 1,
            width = 250, 
            value = bind 'NewAlbumName',
            enabled = bind {
                  key = 'SelectedMode',
                  transform = function( value, fromTable )
                    return value == 'new'
                  end
                }
          }
        }
      }
      local result = LrDialogs.presentModalDialog {
          title = LOC "$$$/Picasa/ExportDialog/Title=Select existing or create new album", 
          contents = contents
        }
      
      if result == 'ok' then
        if properties.SelectedMode == 'existing' then
          log:trace('album=', properties.SelectedAlbum)
          albumID = properties.SelectedAlbum
        else
          local albumName = trim(properties.NewAlbumName) 
          log:trace('Creating new album', albumName)
          albumID = PicasaAPI.createAlbum(albumName, propertyTable.AccountID, propertyTable.AccessToken)
          if albumID == nil then
            LrErrors.throwUserError("Album create fail")
          end
        end
      else
        LrErrors.throwCanceled()
      end
      
    end )
  return albumID
end
      


function PicasaAPI.getAccessAndRefreshToken(propertyTable)
    LrTasks.startAsyncTask( function()
      log:trace('Start getAccessAndRefreshToken for '..prefs.AccessCode)
      local headers = {
        { field = 'Content-Type', value = 'application/x-www-form-urlencoded'}
      }
      -- Trying to get access-token and refresh-token
      local body = string.format(PicasaAPI.googleAuth.accessBody1, prefs.AccessCode, propertyTable.ClientID, propertyTable.ClientSecret)..PicasaAPI.googleAuth.accessBody2
      local response, hdrs = LrHttp.post(PicasaAPI.googleAuth.accessURL, body, headers)
      log:trace('Get Access&Refresh response '..response)
      response =  json.decode(response)
      if hdrs.status ~= 200 then
        LrErrors.throwUserError( formatError( string.format('Get Access&Refresh Error %d: %s', hdrs.status, response.error )) )
      end
      propertyTable.AccessToken = response.access_token
      propertyTable.RefreshToken = response.refresh_token      
      propertyTable.LoginButtonTitle = loginButtonChoice.logout
      log:trace('AccessToken=', propertyTable.AccessToken, 'RefreshToken=', propertyTable.RefreshToken)
    end )
end

function PicasaAPI.refreshToken( propertyTable )
  log:trace( "PicasaAPI.refreshToken" )
  local headers = {
        { field = 'Content-Type', value = 'application/x-www-form-urlencoded'}
      }
  -- Trying to get new access-token with refresh-token
  local body = string.format(PicasaAPI.googleAuth.refreshBody, propertyTable.RefreshToken, propertyTable.ClientID, propertyTable.ClientSecret)
  local response, hdrs = LrHttp.post(PicasaAPI.googleAuth.accessURL, body, headers)
  log:trace('Refresh response '..response)
  response =  json.decode(response)
  if hdrs.status ~= 200 then
    LrErrors.throwUserError( formatError( string.format('Refresh error%d: %s', hdrs.status, response.error )) )
  end
  propertyTable.AccessToken = response.access_token
  log:trace('AccessToken=', propertyTable.AccessToken)
end

function PicasaAPI.notLoggedIn( propertyTable )
  log:trace( "PicasaAPI.notLoggedIn" )

	propertyTable.AccessToken = nil
  propertyTable.RefreshToken = nil  
	
	propertyTable.Account = LOC "$$$/Picasa/ExportDialog/Title/NotLoggedIn=Not logged in"
  propertyTable.AccountID = nil
  propertyTable.AccountInfo = ''
  --propertyTable.CurrentAlbum = nil
  propertyTable.Albums = {}
	propertyTable.LoginButtonTitle = loginButtonChoice.login
	propertyTable.ValidAccount = false
end



function PicasaAPI.updateStatus( propertyTable )
		log:trace( "PicasaAPI.updateStatus" )
    if propertyTable.AccessToken and string.len(propertyTable.AccessToken) > 0 then
      log:trace( "getAccountDetails for "..propertyTable.AccessToken )
      local email, id = PicasaAPI.getAccountUserInfo( propertyTable )
      if email then
        propertyTable.Account = email
        propertyTable.AccountID = id
        propertyTable.LoginButtonTitle = loginButtonChoice.logout
        --[[local info = PicasaAPI.getAccountDetails( propertyTable )
        propertyTable.AccountInfo = LOC ('$$$/Picasa/UserInfo/AccountDetails=^1 albums, used: ^2Gb of ^3Gb',
                string.format("%s", info.totalResults),
                string.format("%.2f", tonumber(string.format("%s", info.quotacurrent)) / 2^30),
                string.format("%.2f", tonumber(string.format("%s", info.quotalimit)) / 2^30)
              )]]--
        propertyTable.ValidAccount = true
      else
        -- Try to refresh access token
        if propertyTable.RefreshToken and string.len(propertyTable.RefreshToken) > 0 then
          PicasaAPI.refreshToken( propertyTable)
        else
          PicasaAPI.notLoggedIn( propertyTable )
        end
      end
    else
      PicasaAPI.notLoggedIn( propertyTable )
    end
end

function PicasaAPI.verifyLogin( propertyTable )
  log:trace( "PicasaAPI.verifyLogin" )
  
  local function updateStatusAsync()
		LrTasks.startAsyncTask(	function()
        PicasaAPI.updateStatus(propertyTable)
    end)
	end

	propertyTable:addObserver( 'AccessToken', updateStatusAsync )
	updateStatusAsync()
	
end


function PicasaAPI.getAccountUserInfo( propertyTable )
    log:trace('PicasaAPI.getAccountUserInfo')
    local headers = {
      { field = 'Authorization', value = 'Bearer '..propertyTable.AccessToken}
    }
    local response, hdrs = LrHttp.get(PicasaAPI.googleAuth.userinfoURL, headers)
    log:trace('Userinfo response '..response)
    response =  json.decode(response)
    if hdrs.status ~= 200 then
      log:trace( formatError( string.format('%d: %s', hdrs.status, response.error )) )
      return nil
    end
    return response.email, response.id
end



function PicasaAPI.getAccountDetails( propertyTable )
    log:trace('PicasaAPI.getAccountDetails')
    local headers = {
      { field = 'Authorization', value = 'Bearer '..propertyTable.AccessToken}
    }
    local response, hdrs = LrHttp.get(string.format(PicasaAPI.googleAuth.userapiURL, propertyTable.AccountID), headers)
    if hdrs.status ~= 200 then
      if response.error ~= nil then
        LrErrors.throwUserError(formatError( string.format('%d: %s', hdrs.status, response.error.message )))
      else
        LrErrors.throwUserError(formatError( string.format('%d: %s', hdrs.status, response)))
      end
    end
    --log:trace('Details response', response)
    local data =  xml.ElementToSimpleTable(response)
    log:trace(string.format('Details response: %s(%s)', data.nickname, data.totalResults))
    return data
end

function PicasaAPI.getPhotoTitle( photo, titletype, pathOrMessage )
	local title
			
	-- Get title according to the options in PhotoTitle section.
	if titletype == 'filename' then
		title = LrPathUtils.leafName( pathOrMessage )
	elseif titletype == 'title' then
		title = photo:getFormattedMetadata('title') 
    if not title or string.len(title) == 0 then
      title = LrPathUtils.leafName( pathOrMessage )
    end
  elseif titletype == 'caption' then
    title = photo:getFormattedMetadata('caption')
  elseif titletype == 'titleAndCaption' then
    title = photo:getFormattedMetadata('title').." "..photo:getFormattedMetadata('caption')
  elseif titletype == 'filenameNoExt' then
		title = LrPathUtils.removeExtension(LrPathUtils.leafName( pathOrMessage ))
	end
				
	return title
end

function PicasaAPI.createAlbum(albumName, userID, accessToken)
  log:trace('PicasaAPI.createAlbum', albumName)
  
  local headers = {
      { field = 'Authorization', value = 'Bearer '..accessToken},
      { field = 'Content-Type', value = 'application/atom+xml'}
    }
  local url = string.format(PicasaAPI.googleAuth.userapiURL, userID)
  
  local body = string.format([[<entry xmlns='http://www.w3.org/2005/Atom'
    xmlns:media='http://search.yahoo.com/mrss/'
    xmlns:gphoto='http://schemas.google.com/photos/2007'>
  <title type='text'>%s</title>
  <category scheme='http://schemas.google.com/g/2005#kind'
    term='http://schemas.google.com/photos/2007#album'></category>
</entry>]], albumName)

  --log:trace(url, inspect(headers), body)
  
  local response, hdrs = LrHttp.post(url, body, headers)
  
  --log:trace('CreateAlbum response:', inspect(response))
  
  if not response then
			if hdrs and hdrs.error then
			LrErrors.throwUserError( formatError( hdrs.error.nativeCode ) )
		end
	end
  
  response =  xml.ElementToSimpleTable(response)
  --log:trace('CreateAlbum response:', inspect(response))
  return tostring(response.id)
  
end

function PicasaAPI.updateAlbum(params, userID, accessToken)
  log:trace('PicasaAPI.updateAlbum', params.name)
  
  local remoteId = params.remoteId
  local name = params.name
  local cmd0
  
  if WIN_ENV == true then
    cmd0 = '"'..LrPathUtils.child( _PLUGIN.path, "curl.exe" )..'"'
  else
    cmd0 = "curl"
  end
  --log:trace(cmd0)
  local cmd1 = string.format([[ -k -X PUT -H "Authorization: Bearer %s" -H "Content-Type: application/atom+xml" -H "If-Match: *"]], accessToken)
  local cmd2 = string.format([[ -d "<entry xmlns='http://www.w3.org/2005/Atom' xmlns:gphoto='http://schemas.google.com/photos/2007' xmlns:media='http://search.yahoo.com/mrss/'><title type='text'>%s</title><gphoto:id>%s</gphoto:id><category scheme='http://schemas.google.com/g/2005#kind' term='http://schemas.google.com/photos/2007#album'></category></entry>"]], name, remoteId)
  local cmd3 = string.format(" https://picasaweb.google.com/data/entry/api/user/%s/albumid/%s", userID, remoteId)
  
  local command = cmd0..cmd1..cmd2..cmd3
  if WIN_ENV == true then
    command = '"'..command..'"'
  end
  
  --log:trace('Executing ', command)
  if LrTasks.execute( command ) ~= 0 then
    LrErrors.throwUserError( 'Failed to update album' )
  end
  
end

function PicasaAPI.deleteAlbum(params, userID, accessToken)
  log:trace('PicasaAPI.deleteAlbum', params.remoteId)
  
  local remoteId = params.remoteId 
  local cmd0
  
  if WIN_ENV == true then
    cmd0 = '"'..LrPathUtils.child( _PLUGIN.path, "curl.exe" )..'"'
  else
    cmd0 = "curl"
  end
  
  local cmd1 = string.format([[ -k -X DELETE -H "Authorization: Bearer %s" -H "Content-Type: application/atom+xml" -H "If-Match: *"]], accessToken)
  local cmd2 = string.format(" https://picasaweb.google.com/data/entry/api/user/%s/albumid/%s", userID, remoteId)
  
  local command = cmd0..cmd1..cmd2
  if WIN_ENV == true then
    command = '"'..command..'"'
  end
  
  --log:trace('Executing ', command)
  if LrTasks.execute( command ) ~= 0 then
    LrErrors.throwUserError( 'Failed to delete album' )
  end
end  

function PicasaAPI.uploadPhoto( params, userID, accessToken )
  log:trace('PicasaAPI.uploadPhoto', params.title)
  
  local albumID = params.albumID
  --log:trace(propertyTable.CurrentAlbum, albumID)
  if albumID == nil then
    LrErrors.throwUserError( "Unknown album")
  end
  
  local url = string.format(PicasaAPI.googleAuth.albumUrl, userID, albumID)
  
  log:trace( string.format('uploading to %s photo T:%s P:%s', url, params.title, params.filePath ))
  
  local data = LrFileUtils.readFile( params.filePath )
  
  local body = string.format([[Media multipart posting
--END_OF_PART
Content-Type: application/atom+xml

<entry xmlns='http://www.w3.org/2005/Atom'>
  <title>%s</title>
  <summary>%s</summary>
  <category scheme="http://schemas.google.com/g/2005#kind"
    term="http://schemas.google.com/photos/2007#photo"/>
</entry>
--END_OF_PART
Content-Type: image/jpeg

%s
--END_OF_PART--]], params.title, params.summary, data)

  local headers = {
      { field = 'Authorization', value = 'Bearer '..accessToken},
      { field = 'Content-Type', value = 'multipart/related; boundary="END_OF_PART"'},
      { field = 'MIME-version', value = '1.0'},
      { field = 'Content-Length', value = #body}
    }
  
  local response, hdrs = LrHttp.post(url, body, headers)

  --log:trace(inspect(response))
	
	if not response then
		if hdrs and hdrs.error then
			LrErrors.throwUserError( formatError( hdrs.error.nativeCode ) )
    end
	end
  
  response =  xml.ElementToSimpleTable(response)
  local uploadedId = tostring(response.id)
  log:trace('Uploaded id:', uploadedId)
  return uploadedId
  
end

function PicasaAPI.updatePhoto( params, userID, accessToken )
  log:trace('PicasaAPI.updatePhoto', params.title)
  
  local photoId = params.photoId
  local ids = PicasaAPI.splitPhotoID(photoId)
  local url = string.format(PicasaAPI.googleAuth.photoPutUrl, userID, ids[2], ids[3])
  log:trace('PUT', url)
  
  local cmd0
  
  if WIN_ENV == true then
    cmd0 = '"'..LrPathUtils.child( _PLUGIN.path, "curl.exe" )..'"'
  else
    cmd0 = "curl"
  end
  local cmd1 = string.format([[ -k -X PUT -H "Authorization: Bearer %s" -H "If-Match: *" -H "Content-Type: image/jpeg"]], accessToken)
  local cmd2 = string.format([[ --data-binary @"%s"]], params.filePath)
  
  local command = cmd0..cmd1..cmd2.." "..url
  if WIN_ENV == true then
    command = '"'..command..'"'
  end
  
  --log:trace('Executing ', command)
  if LrTasks.execute( command ) ~= 0 then
    LrErrors.throwUserError( 'Failed to update photo' )
  end
  
  return ids[3]

end

function PicasaAPI.deletePhoto( params, userID, accessToken )
  log:trace('PicasaAPI.deletePhoto')
  
  local photoId = params.photoId
  local ids = PicasaAPI.splitPhotoID(photoId)
  local url = string.format(PicasaAPI.googleAuth.photoDelUrl, ids[1], ids[2], ids[3])
  log:trace('DELETE', url)
  
  local cmd0
  
  if WIN_ENV == true then
    cmd0 = '"'..LrPathUtils.child( _PLUGIN.path, "curl.exe" )..'"'
  else
    cmd0 = "curl"
  end
  local cmd1 = string.format([[ -k -X DELETE -H "Authorization: Bearer %s" -H "If-Match: *"]], accessToken)
  
  local command = cmd0..cmd1.." "..url
  if WIN_ENV == true then
    command = '"'..command..'"'
  end
  
  --log:trace('Executing ', command)
  if LrTasks.execute( command ) ~= 0 then
    LrErrors.throwUserError( 'Failed to delete photo' )
  end
  
end

function PicasaAPI.commentPhoto(params, userID, accessToken )
  log:trace('PicasaAPI.commentPhoto')

  local photoId = params.photoId
  local ids = PicasaAPI.splitPhotoID(photoId)
  local comment = params.comment
  local url = string.format(PicasaAPI.googleAuth.photoCommentUrl, ids[1], ids[2], ids[3])

  log:trace('POST', url)

  local headers = {
      { field = 'Authorization', value = 'Bearer '..accessToken},
      { field = 'Content-Type', value = 'application/atom+xml'}
    }
  
  local body = string.format([[<entry xmlns='http://www.w3.org/2005/Atom'>
  <content>%s</content>
  <category scheme="http://schemas.google.com/g/2005#kind"
    term="http://schemas.google.com/photos/2007#comment"/>
</entry>]], comment)

  local response, hdrs = LrHttp.post(url, body, headers)
  
  --log:trace('CommentPhoto response:', inspect(response))
  
  if not response then
      if hdrs and hdrs.error then
      LrErrors.throwUserError( formatError( hdrs.error.nativeCode ) )
    end
  end
end

function PicasaAPI.getComments(params, userID, accessToken )
  log:trace('PicasaAPI.getComments')

  local photoId = params.photoId
  local ids = PicasaAPI.splitPhotoID(photoId)
  local url = string.format(PicasaAPI.googleAuth.photoCommentUrl, ids[1], ids[2], ids[3]).."?kind=comment"
  log:trace('GET', url)
  local headers = {
      { field = 'Authorization', value = 'Bearer '..accessToken}
  }

  local response, hdrs = LrHttp.get(url, headers)
  
  --local inspect = require('inspect')
  --log:trace('GetComments response:', inspect(response))
  
  if not response then
      if hdrs and hdrs.error then
      LrErrors.throwUserError( formatError( hdrs.error.nativeCode ) )
    end
  end  

  local data =  xml.ElementToSimpleTable(response)

  local comments = {}

  if data.entry then 
    for i=1, #data.entry do
      local comment = {}
      comment.id = tostring(data.entry[i].id)
      comment.commentText = tostring(data.entry[i].content)
      --comment.dateCreated = tostring(data.entry[i].published)
      comment.username = tostring(data.entry[i].author.user)
      comment.realname = tostring(data.entry[i].author.name)    
      --log:trace(inspect(comment))
      comments[#comments+1] = comment
    end
  end

  return comments

end


function PicasaAPI.constructPhotoID(accountID, albumID, uploadedId)
  return accountID..'_'..albumID..'_'..uploadedId
end

function PicasaAPI.splitPhotoID(photoId)
  return split(photoId, '_')
end

return PicasaAPI