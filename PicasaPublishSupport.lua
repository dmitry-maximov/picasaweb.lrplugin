    -- Lightroom SDK
local LrDialogs = import 'LrDialogs'
local LrView = import 'LrView'
local LrLogger = import 'LrLogger'
local LrTasks = import 'LrTasks'
local LrApplication = import 'LrApplication'
local LrErrors = import 'LrErrors'
local log = LrLogger( 'PicasaLogger' )
--local inspect = require('inspect')

local bind = LrView.bind
local PicasaAPI = require('PicasaAPI')

local PicasaPublishSupport = {}

PicasaPublishSupport.small_icon = 'small_picasaweb.png'
PicasaPublishSupport.titleForPublishedCollection = LOC "$$$/Picasa/TitleForPublishedCollection=Album"
PicasaPublishSupport.titleForPublishedSmartCollection = LOC "$$$/Picasa/TitleForPublishedSmartCollection=Smart Album"
PicasaPublishSupport.titleForGoToPublishedCollection = LOC "$$$/Picasa/TitleForGoToPublishedCollection=Show in Picasa"
PicasaPublishSupport.supportsCustomSortOrder = false
PicasaPublishSupport.disableRenamePublishedCollection = false
--PicasaPublishSupport.titleForPhotoRating = LOC "$$$/Picasa/TitleForPhotoRating=Favorite Count"
--PicasaPublishSupport.publish_fallbackNameBinding = 'fullname'
--PicasaPublishSupport.titleForPublishedCollectionSet = "(something)" -- not used for Flickr plug-in
--function PicasaPublishSupport.getCollectionBehaviorInfo( publishSettings )
--function PicasaPublishSupport.goToPublishedCollection( publishSettings, info )
--function PicasaPublishSupport.goToPublishedPhoto( publishSettings, info )
--function PicasaPublishSupport.didCreateNewPublishService( publishSettings, info )
--function PicasaPublishSupport.didUpdatePublishService( publishSettings, info )
--function PicasaPublishSupport.shouldDeletePublishService( publishSettings, info )
--function PicasaPublishSupport.willDeletePublishService( publishSettings, info )
--function PicasaPublishSupport.shouldDeletePublishedCollection( publishSettings, info )
--function PicasaPublishSupport.shouldDeletePhotosFromServiceOnDeleteFromCatalog( publishSettings, nPhotos )
--function PicasaPublishSupport.endDialogForCollectionSettings( publishSettings, info )
--function PicasaPublishSupport.endDialogForCollectionSetSettings( publishSettings, info )
--function PicasaPublishSupport.updateCollectionSetSettings( publishSettings, info )
--function PicasaPublishSupport.imposeSortOrderOnPublishedCollection( publishSettings, info, remoteIdSequence )
--PicasaPublishSupport.disableRenamePublishedCollectionSet = false -- not used for Flickr sample plug-in
--function PicasaPublishSupport.renamePublishedCollection( publishSettings, info )
--function PicasaPublishSupport.reparentPublishedCollection( publishSettings, info )
--function PicasaPublishSupport.metadataThatTriggersRepublish( publishSettings )
--function PicasaPublishSupport.getRatingsFromPublishedCollection( publishSettings, arrayOfPhotoInfo, ratingCallback )
function PicasaPublishSupport.viewForCollectionSettings( f, publishSettings, info )
  log:trace('viewForCollectionSettings')--, info.collectionSettings.LR_canSaveCollection, info.collectionSettings.LR_canEditName)
  
  local content = nil
  if info.publishedCollection == nil then --adding new collection
    content = f:static_text {
          title = LOC "$$$/Picasa/NewPublishCollectionTitle=New album will be created",
        }
  else
    log:trace( info.name, info.publishedCollection:getName())
  end
  return content
end

function PicasaPublishSupport.viewForCollectionSetSettings( f, publishSettings, info )
  log:trace('viewForCollectionSetSettings', info.name)
  info.collectionSettings.LR_canSaveCollection = false
  return f:static_text {
          title = LOC "$$$/Picasa/UnsupportedSetPublishCollectionTitle=Sorry, Picasa doesn't support Collection Sets",
        }
end

function PicasaPublishSupport.updateCollectionSettings( publishSettings, info )
  log:trace('updateCollectionSettings')
  --log:trace(info.publishedCollection:getName(), info.name)
  PicasaAPI.updateStatus(publishSettings)  -- Check access_token
  if publishSettings.AccessToken == nil then
    LrErrors.throwUserError ( "Google auth failed")
  end
  --log:trace(publishSettings.AccountID, publishSettings.AccessToken)
  LrTasks.startAsyncTask( function()
      local remoteId = info.publishedCollection:getRemoteId()
      if remoteId == nil then --Creating new album
        local albumID = PicasaAPI.createAlbum(info.name, publishSettings.AccountID, publishSettings.AccessToken)
        log:trace(string.format('Created album %s id=%s', info.name, albumID))
        LrApplication.activeCatalog():withWriteAccessDo( 'SetRemoteId', function( context ) 
          info.publishedCollection:setRemoteId(albumID)
          info.publishedCollection:setRemoteUrl(string.format(PicasaAPI.googleAuth.albumPlusUrl, publishSettings.AccountID, albumID))
        end)
      else --Updating album
        log:trace(string.format("Updating album rid=%s", remoteId))
        PicasaAPI.updateAlbum({remoteId=remoteId, name=info.name}, publishSettings.AccountID, publishSettings.AccessToken)
      end
  end)
  
end

function PicasaPublishSupport.shouldReverseSequenceForPublishedCollection( publishSettings, collectionInfo )
  return false
end

function PicasaPublishSupport.validatePublishedCollectionName( proposedName )
  return true
end



function PicasaPublishSupport.deletePublishedCollection( publishSettings, info )
  log:trace('deletePublishedCollection', info.remoteId)
  if info.remoteId == nil then
    error( "Nil remote id")
  end
  PicasaAPI.updateStatus(publishSettings)  -- Check access_token
  if publishSettings.AccessToken == nil then
    LrErrors.throwUserError ( "Google auth failed")
  end
  PicasaAPI.deleteAlbum({remoteId=info.remoteId}, publishSettings.AccountID, publishSettings.AccessToken)
end



function PicasaPublishSupport.deletePhotosFromPublishedCollection( publishSettings, arrayOfPhotoIds, deletedCallback )
  PicasaAPI.updateStatus(publishSettings)  -- Check access_token
  if publishSettings.AccessToken == nil then
    LrErrors.throwUserError ( "Google auth failed")
  end
  for i, photoId in ipairs( arrayOfPhotoIds ) do
    log:trace('Delete photo', photoId)
		PicasaAPI.deletePhoto( {photoId=photoId}, publishSettings.AccountID, publishSettings.AccessToken)
		deletedCallback( photoId )
	end
end

function PicasaPublishSupport.canAddCommentsToService( publishSettings )
  return true
end

function PicasaPublishSupport.getCommentsFromPublishedCollection( publishSettings, arrayOfPhotoInfo, commentCallback )
  PicasaAPI.updateStatus(publishSettings)  -- Check access_token
  if publishSettings.AccessToken == nil then
    LrErrors.throwUserError ( "Google auth failed")
  end
  for i, photoInfo in ipairs( arrayOfPhotoInfo ) do

		local comments = PicasaAPI.getComments({photoId=photoInfo.remoteId}, publishSettings.AccountID, publishSettings.AccessToken)
		local commentList = {}
		
		if comments and #comments > 0 then
			for _, comment in ipairs( comments ) do
				table.insert( commentList, {
								commentId = comment.id,
								commentText = comment.commentText,
								--dateCreated = comment.datecreate,
								username = comment.username,
								realname = comment.realname
							} )
        log:trace('Comment', comment.commentText)
			end			
		end	

    --log:trace(inspect(commentList))

		commentCallback{ publishedPhoto = photoInfo, comments = commentList }					    
  end

end

function PicasaPublishSupport.addCommentToPublishedPhoto( publishSettings, remotePhotoId, commentText )
  PicasaAPI.updateStatus(publishSettings)  -- Check access_token
  if publishSettings.AccessToken == nil then
    LrErrors.throwUserError ( "Google auth failed")
  end
  PicasaAPI.commentPhoto({photoId=remotePhotoId, comment=commentText}, publishSettings.AccountID, publishSettings.AccessToken)
  return true
end

return PicasaPublishSupport
