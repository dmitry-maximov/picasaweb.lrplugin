return {

    LrSdkVersion = 3.0,
    LrSdkMinimumVersion = 3.0, -- minimum SDK version required by this plug-in

    LrToolkitIdentifier = 'com.xururu.lightroom.export.picasa',
    LrPluginName = LOC "$$$/Picasa/PluginName=Picasa",
    
    LrExportServiceProvider = {
        title = LOC "$$$/Picasa/PluginTitle=PicasaWeb",
        file = 'PicasaExportServiceProvider.lua',
    },
    
    VERSION = { major=0, minor=5, revision=1 },

}