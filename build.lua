local isWindows = string.find(os.getenv("PATH"),";",1,true) ~= nil

function quoteCmd(cmd)
  if isWindows then
    return '"'..cmd..'"'
  else
    return cmd
  end
end

local sources = {"defs.lua","dkjson.lua","Info.lua","PicasaAPI.lua",
"PicasaExportServiceProvider.lua","PicasaPublishSupport.lua","xml.lua"}
local data = {"curl.exe", "README.md", "small_picasaweb.png"}

local buildDir
if #arg > 0 then
  buildDir = arg[1]
else 
  buildDir = "build"
end  

local baseName = string.gsub(buildDir, "(.*[\\/])(.*)", "%2")

local version
if #arg > 1 then
  version = arg[2]
else 
  version = "0.5.1"
end  

local mkdir = 'mkdir "%s"'
local build = 'luac -o "%s/%s" "%s"'
local rmdir, copy
if isWindows then
  rmdir = 'rmdir /q /s "%s"'
  copy = 'copy "%s" "%s/%s"'
else
  rmdir = 'rm -rf "%s"'
  copy = 'cp "%s" "%s/%s"'
end

os.execute(quoteCmd(string.format(rmdir, buildDir)))
os.execute(quoteCmd(string.format(mkdir, buildDir)))

for i = 1, #sources do
  os.execute(quoteCmd(string.format(build, buildDir, sources[i], sources[i])))
end

for i = 1, #data do
  os.execute(quoteCmd(string.format(copy, data[i], buildDir, data[i])))
end

os.execute(quoteCmd(string.format('cd "%s" && cd .. && zip "%s.%s.zip" %s/*', buildDir, baseName, version, baseName)))

