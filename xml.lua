local LrXml = import 'LrXml'
local LrLogger = import 'LrLogger'
local log = LrLogger( 'PicasaLogger' )
local filelog = LrLogger ('PicasaFileLogger' )
--local inspect = require('inspect')

local xml = {}

local simpleXmlMetatable = {
	__tostring = function( self ) return self._value end
}

--[[local function deepcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in next, orig, nil do
            copy[deepcopy(orig_key)] = deepcopy(orig_value)
        end
        setmetatable(copy, deepcopy(getmetatable(orig)))
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end]]--

function xml.traverse( node )

	local nodetype = string.lower( node:type() )

	if nodetype == 'element' then

		local element = setmetatable( {}, simpleXmlMetatable )
		element._name = node:name()
		element._value = node:text()
		
		local count = node:childCount()

		for i = 1, count do
			local name, value = xml.traverse( node:childAtIndex( i ) )
			if name and value then
        if name == 'entry' then
          if element[ name ] == nil then
            element[name] = {}
          end
          element[name][#element[name]+1] = value
        else
          element[ name ] = value
        end
			end
		end

		if nodetype == 'element' then
			for k, v in pairs( node:attributes() ) do
				element[ k ] = v.value
			end
		end
		
		return element._name, element

	end

end

function xml.ElementToSimpleTable( xmlString )
	local _, value = xml.traverse( LrXml.parseXml( xmlString ) )
	return value
end

return xml