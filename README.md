# Adobe Lightroom export to Picasa plugin

## Summary

Easy export photoes from Adobe Lightroom 4,5 to Picasa.

## Installation

Clone repository to picasaweb.lrplugin to your Lightroom plugins directory.
In Lightroom, open File>Plug-in Manager... and add picasaweb.lrplugin.

If everything is ok, plugin Picasa should be Installed and running.

## Usage

Select exported photoes and press Export, then select Export To: Picasa.
In Picasa Account section press Login, then press Get Google Access code button - you will be redirected to login page via browser.
Copy Access code from browser to dialog, and if everything is ok Account Info and albums list will be displayed.

## Known bugs