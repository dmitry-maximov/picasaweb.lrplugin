photoTitleChoice = {
    title = LOC "$$$/Picasa/ExportDialog/Title/Title=Title",
    caption = LOC "$$$/Picasa/ExportDialog/Title/Caption=Caption",
    titleAndCaption = LOC "$$$/Picasa/ExportDialog/Title/TitleAndCaption=Title and caption",
    filename = LOC "$$$/Picasa/ExportDialog/Title/Filename=Filename",
    filenameNoExt = LOC "$$$/Picasa/ExportDialog/Title/FilenameNoExt=Filename (no extension)",
}

loginButtonChoice = {
    login = LOC "$$$/Picasa/ExportDialog/Login=Login",
    logout = LOC "$$$/Picasa/ExportDialog/Logout=Logout"
}

